FROM python:3.4-alpine
ADD . /code
WORKDIR /code
RUN pip install -r reqirements.txt
CMD ["python","app.py"]
